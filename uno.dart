import 'dart:io';

void main() {
  print('What is your name?');
  String? userName = stdin.readLineSync();

  print('What is your favourite app?');
  String? userFavApp = stdin.readLineSync();

  print('What city are you from?');
  String? userCity = stdin.readLineSync();

  print('$userName from $userCity likes the $userFavApp app');
}
